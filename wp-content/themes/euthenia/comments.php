<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Euthenia
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
	<div class="section drop-shadow rounded mt-4 over-hide">
	  <div class="post-comm-box background-dark over-hide">
		<h5 class="mb-3">
			<?php
			$euthenia_comment_count = get_comments_number();
			if ( '1' === $euthenia_comment_count ) {
				/* translators: %s: post title */
				printf( _x( '1 comment', 'comments title', 'euthenia' ), get_the_title() );
			} else {
				printf(
					/* translators: 1: number of comments, 2: post title */
					_nx(
						'%1$s comment',
						'%1$s comments',
						$euthenia_comment_count,
						'comments title',
						'euthenia'
					),
					number_format_i18n( $euthenia_comment_count ),
					get_the_title()
				);
			}
			?>
		</h5><!-- .comments-title -->		
		<div class="separator-wrap pt-3 pb-4">	
			<span class="separator"><span class="separator-line dashed"></span></span>
		</div>
		<?php wp_list_comments( 'callback=euthenia_comment_list' ); ?>
	  </div>
	</div>
		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) : ?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'euthenia' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	// Custom comments_args here: https://codex.wordpress.org/Function_Reference/comment_form
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$comments_args = array(		
        'title_reply'=> ''.esc_html__('Leave a comment','euthenia').'',
	  	'fields' => apply_filters( 'comment_form_default_fields', array(
	    	'author' =>
			'<div class="subscribe-box mt-3"><input id="author" name="author" class="hover-target" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" placeholder="'.esc_attr__('Your Name *','euthenia').'" class="hover-target" size="30"' . $aria_req . ' /></div>',

		    'email' =>
		      '<div class="subscribe-box mt-4"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" placeholder="'.esc_attr__('Email *','euthenia').'" class="hover-target" size="30"' . $aria_req . ' /></div>',

		    'url' =>
		      '<div class="subscribe-box mt-4"><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .'" placeholder="'.esc_attr__('Website','euthenia').'" class="hover-target" size="30" /></div>', 

    	)),

		'comment_field' => '<div class="subscribe-box mt-4"><textarea id="comment" class="hover-target" name="comment" placeholder="'.esc_attr__('Comment *','euthenia').'" cols="45" rows="8" aria-required="true"></textarea></div>',

         'label_submit' => esc_html__( 'submit comment', 'euthenia' ),
         'class_submit'      => 'btn-long submit-comment mt-4 hover-target',
	  );	
	?>
<div class="section drop-shadow rounded mt-4 over-hide">			 
	<div class="post-comm-box background-dark over-hide ajax-form">
		<?php comment_form( $comments_args ); ?>
	</div>
</div>

<!-- #comments -->
