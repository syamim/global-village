<?php
/*
 * Template Name: Canvas
 * Description: A Page Template with a Page Builder design.
 */
$ptext = get_post_meta(get_the_ID(), 'parallax_text', true);
get_header(); ?>
<?php if($ptext!=''){ ?><div class="shadow-title parallax-top-shadow"><?php echo esc_attr($ptext); ?></div><?php } ?>
<?php if (have_posts()){ ?>
	
		<?php while (have_posts()) : the_post()?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	
	<?php }else {
		esc_html_e('Page Canvas For Page Builder', 'euthenia'); 
	}?>

<?php get_footer(); ?>