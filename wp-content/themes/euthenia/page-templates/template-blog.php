<?php
/**
 * Template Name: Blog
 */
$ptext = rwmb_meta('parallax_text', "type=text", get_option( 'page_for_posts' ));
$sub = rwmb_meta('subtitle', "type=text", get_option( 'page_for_posts' ));
get_header(); ?>
<!-- subheader begin -->
<?php if($ptext!=''){ ?><div class="shadow-title parallax-top-shadow"><?php echo esc_attr($ptext); ?></div><?php } ?>
<div class="section padding-page-top padding-bottom over-hide z-bigger">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">
            <div class="col-12 parallax-fade-top">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3">
                <p><?php echo esc_attr($sub); ?></p>
            </div>
        </div>
    </div>
</div>
<!-- subheader close -->

<!-- content begin -->
<div class="section padding-bottom-big z-bigger over-hide">
    <div class="container z-bigger">
        <div class="row page-title px-5 px-xl-2">

            <?php if(euthenia_get_option('blog_layout')== 'sidebar-content'){ ?>
                <?php get_sidebar(); ?>
            <?php } ?>

            <div class="<?php if( euthenia_get_option('blog_layout') == 'full-content' ){ echo 'col-12'; }else{ echo 'col-lg-8'; } ?>">
                <ul class="case-study-wrapper vertical-blog">
                <?php if(have_posts()) : ?>  
                    <?php 
                        $args = array(    
                          'paged' => $paged,
                          'post_type' => 'post',
                        );
                        $wp_query = new WP_Query($args);
                        while ($wp_query -> have_posts()): $wp_query -> the_post();                         
                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'template-parts/content', get_post_format() );
                    ?> 
                <?php endwhile;?>         
                <?php else: ?>
                    <h1><?php esc_html_e('Nothing Found Here!', 'euthenia'); ?></h1>
                <?php endif; ?>   
                </ul><!-- #main -->
            </div><!-- #primary -->
            
            <?php if(euthenia_get_option('blog_layout')== 'content-sidebar'){ ?>
                <?php get_sidebar(); ?>
            <?php } ?>

        </div>
    </div>  
</div>
<?php if(have_posts()) : ?>  
<ul class="case-study-images">
        <?php 
            $args = array(    
              'paged' => $paged,
              'post_type' => 'post',
            );
            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()): $wp_query -> the_post();                         
            /*
             * Include the Post-Format-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
             */
            get_template_part( 'template-parts/content-thumbnail', get_post_format() );
        ?> 
    <?php endwhile;?>   
</ul><!-- #main -->      
<?php endif; ?>   
<?php if ($wp_query->max_num_pages > 1) { ?>
<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
    <div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"><?php esc_html_e('Our Stories','euthenia'); ?></div>
    <div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s">
        <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines custom-footer-lines">
                    <?php previous_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_prev')).'</h4>' ); ?>
                    <?php next_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_next')).'</h4>' ); ?>
                </div>
            </div>
        </div>
    </div>      
</div>
<?php } ?>     

<!-- content close -->

<?php get_footer(); ?>