<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Euthenia
 */

get_header();
?>
<div class="shadow-title parallax-top-shadow"><?php esc_html_e('Search','euthenia'); ?></div>
<div class="section padding-page-top padding-bottom over-hide z-bigger">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">
			<div class="col-12 parallax-fade-top">
				<?php if ( have_posts() ) : ?>
                    <h1><?php printf( esc_html__( 'Search Results for: %s', 'euthenia' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                <?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="section padding-bottom-big z-bigger over-hide">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">

			<div class="content-area <?php euthenia_content_columns(); ?>">
				<ul class="case-study-wrapper vertical-blog">

				<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

				</ul><!-- #main -->
			</div><!-- #primary -->

				<?php get_sidebar(); ?>
			
		</div>
	</div>	
</div>

<?php if ( have_posts() ) : ?>
<ul class="case-study-images">

		<?php /* Start the Loop */
		 while ( have_posts() ) :
			the_post();

			/*
			 * Include the Post-Type-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
			 */
			get_template_part( 'template-parts/content-thumbnail', get_post_type() );

		endwhile;

	?>
	
</ul>
<?php endif; ?>

<?php if ($wp_query->max_num_pages > 1) { ?>
<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
	<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"><?php echo esc_attr(euthenia_get_option('shadow_pagi_blog')); ?></div>
	<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s">
		<div class="row">
			<div class="col-12 text-center z-bigger py-5">
				<div class="footer-lines">
					<?php previous_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_prev')).'</h4>' ); ?>
                    <?php next_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_next')).'</h4>' ); ?>
				</div>
			</div>
		</div>
	</div>		
</div>
<?php } ?>
<?php
get_footer();

