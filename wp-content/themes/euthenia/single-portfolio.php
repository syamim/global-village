<?php 
    get_header(); 
?>

<div class="shadow-title parallax-top-shadow"><?php the_title(); ?></div>
<?php while (have_posts()) : the_post(); ?>

    <?php the_content(); ?>

<?php endwhile; ?>
<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">		
	<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"><?php the_title(); ?></div>			
	<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s">
		<div class="row">
			<div class="col-12 text-center z-bigger py-5">	
				<div class="footer-lines custom-footer-lines">
				<?php echo previous_post_link('%link', __('<h4>'.esc_html__('Prev Project','euthenia').'</h4>', 'euthenia'), $post->max_num_pages); ?>
				<?php echo next_post_link('%link', __('<h4>'.esc_html__('Next Project','euthenia').'</h4>', 'euthenia'), $post->max_num_pages); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>