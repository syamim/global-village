<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Euthenia
 */

if ( euthenia_get_layout() === 'full-content' ) {
	return;
}

$sidebar = 'primary';

if ( ! is_active_sidebar( $sidebar ) ) {
	return;
}
?>

<div id="primary-sidebar" class="widget-area primary-sidebar col-lg-4 mt-4 mt-lg-0">
	<div class="sidebar-box background-dark drop-shadow rounded">
		<?php dynamic_sidebar( $sidebar ); ?>
	</div>
</div><!-- #secondary -->
