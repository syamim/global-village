<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Euthenia
 */

get_header();
?>
<?php if(euthenia_get_option('text_shadow_top')!=''){ ?><div class="shadow-title parallax-top-shadow"><?php echo esc_attr(euthenia_get_option('text_shadow_top')); ?></div><?php } ?>
<div class="section padding-page-top padding-bottom over-hide z-bigger">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">
			<div class="col-12 parallax-fade-top">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3">
				<?php while ( have_posts() ) : the_post(); ?>
				<p><?php esc_html_e('by ','euthenia'); ?> <?php the_author(); ?>, <?php the_time( get_option( 'date_format' ) ); ?></p>
				<?php endwhile; ?>	
			</div>
		</div>
	</div>
</div>
<div class="section padding-bottom-big z-bigger over-hide">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">
			
			<div id="primary" class="content-area <?php euthenia_content_columns(); ?>">
				<div class="section drop-shadow rounded">
					<div class="post-box background-dark over-hide">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content-single', get_post_type() );

					?>
					</div>
				</div><!-- #main -->
				<?php 
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

			 	?>
			 	<?php endwhile; // End of the loop. ?>
			</div><!-- #primary -->

				<?php get_sidebar(); ?>
		</div>
	</div>	
</div>

<?php if(euthenia_get_option('single_pagi')==true){ ?>
<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
	<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"><?php echo esc_attr(euthenia_get_option('shadow_pagi')); ?></div>
	<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s">
		<div class="row">
			<div class="col-12 text-center z-bigger py-5">
				<div class="footer-lines custom-footer-lines">
					<?php previous_post_link( '%link', _x( '<h4>'.esc_attr(euthenia_get_option('text_single_prev')).'</h4>', 'euthenia' ) ); ?> 
	            	<?php next_post_link( '%link', _x( '<h4>'.esc_attr(euthenia_get_option('text_single_next')).'</h4>', 'euthenia' ) ); ?>
				</div>
			</div>
		</div>
	</div>		
</div>
<?php } ?>
<?php
get_footer();
