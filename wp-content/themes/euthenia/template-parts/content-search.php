<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Euthenia
 */

?>

<li id="post-<?php the_ID(); ?>" class="case-study-name mb-5">
		<?php the_title( '<a class="hover-target animsition-link" href="' . esc_url( get_permalink() ) . '" rel="bookmark"><h4 class="mb-3">', '</h4></a>' ); ?>
		<div class="row">
			<div class="col-lg-6 entry-summary">
				<?php the_excerpt(); ?>
				<p class="lead pl-0 pl-md-5 mb-0"><em><?php the_time( get_option( 'date_format' ) ); ?></em></p>
				<a href="<?php the_permalink(); ?>" class="hover-target animsition-link"><div class="go-to-post"></div></a>
			</div>
		</div>

</li><!-- #post-<?php the_ID(); ?> -->
