<?php
/**
 * Template part for displaying single post content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Industro
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
     <?php the_post_thumbnail( 'full', array( 'class' => 'blog-home-img' ) ); ?>

    <div class="padding-in">
        <?php the_content(); ?>

        <div class="separator-wrap pt-4 pb-4"> 
            <span class="separator"><span class="separator-line dashed"></span></span>
        </div>
        <?php if(get_the_tag_list()) { ?>
            <?php  echo get_the_tag_list('<div class="tag-list">','','</div>'); ?>
            <div class="separator-wrap pt-4 pb-4">  
                <span class="separator"><span class="separator-line dashed"></span></span>
            </div>
        <?php } ?>
        <div class="author-wrap">
            <?php echo get_avatar($comment,$size='60',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=180' ); ?>
            <p> <?php esc_html_e('by ','euthenia'); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="hover-target"><strong><?php the_author(); ?></strong></a></p>
        </div>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
