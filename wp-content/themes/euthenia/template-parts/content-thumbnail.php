<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Euthenia
 */

?>

<li id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	<div class="img-hero-background blog-back-image" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>');"></div> 
</li>