<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Euthenia
 */

get_header();
?>
<div class="shadow-title parallax-top-shadow"><?php esc_html_e('404','euthenia'); ?></div>
<div class="section error-404 padding-page-top padding-bottom over-hide z-bigger text-center">
	<div class="container z-bigger">
		<div class="row px-5 px-xl-2">
			<div class="col-12">
		        <h1><?php esc_html_e( '404', 'euthenia' ); ?></h1>
		        <h2><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'euthenia' ); ?></h2>
		        <div class="page-content">
		            <p><?php wp_kses( _e( 'Page you are looking for counld not be found.</br> Search or back to ', 'euthenia' ), wp_kses_allowed_html('post')  ); ?><a class="hover-target" href="<?php echo esc_url( home_url("/") ); ?>"><?php esc_html_e('home','euthenia'); ?></a></p>

		            <?php get_search_form(); ?>

	        	</div><!-- .page-content -->
		    </div><!-- .error-404 -->
		</div>
	</div>
</div>
<?php
get_footer();
