<?php

// Intro Section (euthenia)
if(function_exists('vc_map')){
    vc_map( array(
        "name" => esc_html__("OT Section Title", 'euthenia'),
        "base" => "introsec",
        "class" => "",
        "category" => 'Euthenia Element',
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'euthenia'),
                "param_name" => "title",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Subtitle", 'euthenia'),
                "param_name" => "sub",
            ),
        )));
}

// Section Footer (euthenia)
if(function_exists('vc_map')){
    vc_map( array(
        "name" => esc_html__("OT Section Footer", 'euthenia'),
        "base" => "ftsec",
        "class" => "",
        "category" => 'Euthenia Element',
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Parallax Big Text", 'euthenia'),
                "param_name" => "text",
            ),
            array(
                "type" => "vc_link",
                "heading" => esc_html__("Add Link", 'euthenia'),
                "param_name" => "link",
            ),
            array(
                "type" => "checkbox",
                "heading" => esc_html__("Animation", 'euthenia'),
                "param_name" => "anim",
            ),
        )));
}

// Video Player
if(function_exists('vc_map')){
    vc_map( array(
        "name" => esc_html__("OT Video Player", 'euthenia'),
        "base" => "videopl",
        "class" => "",
        "category" => 'Euthenia Element',
        "params" => array(
            array(
                "type" => "attach_image",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Image", 'euthenia'),
                "param_name" => "photo",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Link Video", 'euthenia'),
                "param_name" => "link",
            ),
        )));
}

// Portfolio Slice
if(function_exists('vc_map')){
   vc_map( array(
   "name" => esc_html__("OT Portfolio Slice", 'euthenia'),
   "base" => "folioslice",
   "class" => "",
   "icon" => "icon-st",
   "category" => 'Euthenia Element',
   "params" => array( 
      array(
         "type" => "textfield",
         "holder" => "div",
         "class" => "",
         "heading" => esc_html__("Number Show", 'euthenia'),
         "param_name" => "num",
         "value" => "9",
      ),
    )));
}

// Portfolio Filter
if(function_exists('vc_map')){
    vc_map( array(
        "name" => esc_html__("OT Portfolio Filter", 'euthenia'),
        "base" => "portfoliof",
        "class" => "",
        "icon" => "icon-st",
        "category" => 'Euthenia Element',
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Show All Text", 'euthenia'),
                "param_name" => "all",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Show Number Projects", 'euthenia'),
                "param_name" => "num",
            ),
            array(
                "type" => "checkbox",
                "heading" => esc_html__("Disable Filter", 'euthenia'),
                "param_name" => "filter",
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Column", 'euthenia'),
                "param_name" => "col",
                "value" => array(
                    esc_html__('2 Columns', 'euthenia')     => '',
                    esc_html__('3 Columns', 'euthenia')     => 'portfolio-3col',
                    esc_html__('4 Columns', 'euthenia')     => 'portfolio-4col',
                ),
            ),
        )));
}

//Portfolio Slider
if(function_exists('vc_map')){
    vc_map( array(
        "name" => esc_html__("OT Portfolio Slider", 'euthenia'),
        "base" => "projrelated",
        "class" => "",
        "icon" => "icon-st",
        "category" => 'Euthenia Element',
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number Show Projects", 'euthenia'),
                "param_name" => "num",
                "value" => "",
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "heading" => esc_html__('Style Title', 'euthenia'),
                "param_name" => "style",
                "value" => array(
                    esc_html__('Title Center', 'euthenia')   => 'center',
                    esc_html__('Title Bottom', 'euthenia')     => 'bottom',
                ),
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "heading" => esc_html__('Style Image', 'euthenia'),
                "param_name" => "imgst",
                "value" => array(
                    esc_html__('Fullscreen', 'euthenia')   => 'fullscreen',
                    esc_html__('Not Fullscreen', 'euthenia')     => 'nofull',
                ),
            ),
        )));
}
