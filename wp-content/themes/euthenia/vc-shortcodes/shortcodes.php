<?php

// Section Title (euthenia)
add_shortcode('introsec','introsec_func');
function introsec_func($atts, $content = null){
    extract(shortcode_atts(array(
        'title'     =>  '',
        'sub'       =>  '',
    ), $atts));
    ob_start(); ?>

    <div class="row page-title px-5 px-xl-2">
        <div class="col-12 parallax-fade-top">
            <h1><?php echo esc_attr($title); ?></h1>
        </div>
        <?php if($sub!=''){ ?>
        <div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3">
            <p><?php echo esc_attr($sub); ?></p>
        </div>
        <?php } ?>
    </div>

    <?php
    return ob_get_clean();
}

// Section Footer (euthenia)
add_shortcode('ftsec','ftsec_func');
function ftsec_func($atts, $content = null){
    extract(shortcode_atts(array(
        'text'       =>  '',
        'link'       =>  '',
        'anim'       =>  '',
    ), $atts));
    $url    = vc_build_link( $link );
    ob_start(); ?>
    <?php if($text!=''){ ?><div class="shadow-on-footer" <?php if($anim==true){ ?> data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"<?php } ?>><?php echo esc_attr($text); ?></div><?php } ?>
    <div class="container" <?php if($anim==true){ ?> data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s"<?php } ?>>
        <div class="row">
            <div class="col-12 text-center z-bigger py-5">
                <div class="footer-lines">
                    <?php if ( strlen( $link ) > 0 && strlen( $url['url'] ) > 0 ) {
                        echo '<a class="hover-target animsition-link" href="' . esc_attr( $url['url'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '"><h4>' . esc_attr( $url['title'] ) .'</h4></a>';
                    } ?>
                </div>
            </div>
        </div>
    </div>   

    <?php
    return ob_get_clean();
}

// Video Player (use)
add_shortcode('videopl', 'videopl_func');
function videopl_func($atts, $content = null){
    extract(shortcode_atts(array(
        'photo'       =>  '',
        'link'        =>  '',
    ), $atts));
    $img     = wp_get_attachment_image_src($photo,'full');
    $img     = $img[0];
    ob_start(); ?>

    <div class="video-section hover-target">
        <figure class="vimeo"> 
            <a href="<?php echo esc_attr($link); ?>" class="">
                <img src="<?php echo esc_url($img); ?>" alt=""/>
            </a>
        </figure>
    </div>  

    <?php
    return ob_get_clean();
}

// Portfolio Slice (euthenia)
add_shortcode('folioslice', 'folioslice_func');
function folioslice_func($atts, $content = null){
  extract(shortcode_atts(array(
    'num'   =>  '9',
  ), $atts));
  ob_start(); ?>

    <div class="row px-5 px-xl-2">

        <?php 
        $i=0;
            $args = array(   
              'post_type' => 'portfolio',   
              'posts_per_page' => $num,
            );  
            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post(); $i++;
            $cates = get_the_terms(get_the_ID(),'categories');
            $cate_name ='';
              foreach((array)$cates as $cate){
                if(count($cates)>0){
                  $cate_name .= $cate->name.'';    
                } 
            }
            $image = wp_get_attachment_url(get_post_thumbnail_id());
        ?>
        <div class="<?php if($i%2==0){ echo 'offset-md-3 ';}elseif($i%4==3){ echo ' offset-md-6 ';} ?>col-md-6 img-slice-wrap mb-5 over-hide">
            <a href="<?php the_permalink(); ?>" class="hover-target animsition-link hover-portfolio-box">
                <div class="scroll-img" style="background-image: url(<?php echo esc_url($image); ?>);"></div>
                <p><?php echo esc_attr($cate_name); ?></p>
                <h4><?php the_title(); ?></h4>
            </a>
        </div>
        <?php endwhile; ?>
    </div>
<?php
    return ob_get_clean();
}

// Portfolio Filter
add_shortcode('portfoliof', 'portfoliof_func');
function portfoliof_func($atts, $content = null){
    extract(shortcode_atts(array(
        'all'       =>  '',
        'num'       =>  '-1',
        'col'       =>  '',
        'filter'    =>  '',
    ), $atts));
    ob_start(); ?>
    <?php if($filter!=true){ ?>
    <div class="container">
        <div class="row px-5 px-xl-2">
            <div class="col-md-12 mb-4">
                <div id="portfolio-filter" class="portfolio-filter">
                    <ul id="filter">
                        <?php if($all) { ?><li><a href="#" data-filter="*" class="current hover-target"><?php echo esc_html($all); ?></a></li><?php } ?>
                        <?php
                        $categories = get_terms('categories');
                        foreach( (array)$categories as $categorie){
                            $cat_name = $categorie->name;
                            $cat_slug = $categorie->slug;
                            ?>
                            <li><a href="#" data-filter=".<?php echo esc_attr( $cat_slug ); ?>" class="hover-target"><?php echo esc_html( $cat_name ); ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div id="projects-grid">
        <?php
        $args = array(
            'post_type' => 'portfolio',
            'posts_per_page' => $num,
        );
        $wp_query = new WP_Query($args);
        while ($wp_query -> have_posts()) : $wp_query -> the_post();
            $cates = get_the_terms(get_the_ID(),'categories');
            $cate_name ='';
            $cate_slug = '';
            foreach((array)$cates as $cate){
              if(count($cates)>0){
                $cate_name .= $cate->name.' ';
                $cate_slug .= $cate->slug .' ';     
              } 
          }
        ?>
            <a href="<?php the_permalink(); ?>" class="hover-target animsition-link">
                <div class="portfolio-box <?php echo esc_attr($cate_slug.$col); ?>">
                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                    <div class="portfolio-mask"></div>
                    <p><?php echo esc_attr($cate_name); ?></p>
                    <h4><?php the_title(); ?></h4>
                </div>
            </a>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>

    <?php
    return ob_get_clean();
}

//Portfolio Slider (use)
add_shortcode('projrelated','projrelated_func');
function projrelated_func($atts, $content = null){
    extract(shortcode_atts(array(
        'num'       =>  '',
        'style'     =>  'center',
        'imgst'     =>  'fullscreen',
    ), $atts));
    ob_start();
    ?>
    <div class="section full-height over-hide swiper-container z-bigger" id="hero-slider">
        <ul class="swiper-wrapper case-study-wrapper">
            <?php

            $args = array(
                'post_type' => 'portfolio',
                'posts_per_page' => $num,
            );

            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post();
            ?>
            <li class="swiper-slide <?php if($style=='bottom'){echo 'bottom-name';} ?> full-height case-study-name">
                <a href="<?php the_permalink(); ?>" class="hover-target animsition-link">
                    <h1><?php the_title(); ?></h1>
                </a>
            </li>
        <?php endwhile; ?>
        </ul>
        
        <!-- Background Images -->
        <div class="swiper-scrollbar<?php if($style=='bottom'){echo ' bottom-name';} ?>"></div>
    </div>
    <ul class="case-study-images">
        <?php
            $i=0;
            $args = array(
                'post_type' => 'portfolio',
                'posts_per_page' => $num,
            );

            $wp_query = new WP_Query($args);
            while ($wp_query -> have_posts()) : $wp_query -> the_post(); $i++;
        ?>
        <li>
            <div class="<?php if($imgst=='fullscreen'){echo 'img-hero-background no-blur';}else{echo 'img-hero-background-over';} ?>" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>');"></div> 
            <h2 class="<?php if($style=='bottom'){echo 'bottom-name ';} ?><?php if($imgst=='fullscreen'){echo 'fullscreen-image no-blur';}else{echo 'no-blur-image';} ?>"><?php the_title(); ?></h2>
            <div class="hero-number<?php if($style=='bottom'){echo ' bottom-name';} ?>"><?php if($i<10){echo '0';} ?><?php echo esc_attr($i); ?></div> 
            <?php if($i==1){ ?><div class="hero-number-fixed<?php if($style=='bottom'){echo ' bottom-name';} ?>"><?php if($num<10){echo '0';} ?><?php echo esc_attr($num); ?></div> <?php } ?>
        </li>
        <?php endwhile; wp_reset_postdata();?>
    </ul>
    <?php
    return ob_get_clean();
}
