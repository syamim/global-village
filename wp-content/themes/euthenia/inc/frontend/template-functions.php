<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Euthenia
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function euthenia_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'euthenia_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function euthenia_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'euthenia_pingback_header' );

if ( ! function_exists( 'euthenia_get_layout' ) ) :
	function euthenia_get_layout() {
		// Get layout.
		if( is_page() && !is_home() && function_exists('rwmb_meta') ) {
			$page_layout = rwmb_meta('page_layout');
		}elseif( is_single() ){
			$page_layout = euthenia_get_option( 'single_post_layout' );
		}else{
			$page_layout = euthenia_get_option( 'blog_layout' );
		}

		return $page_layout;
	}
endif;

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! function_exists( 'euthenia_content_columns' ) ) :
	function euthenia_content_columns() {

		$blog_content_width = array();

		// Check if layout is one column.
		if ( 'content-sidebar' === euthenia_get_layout() ) {
			$blog_content_width[] = 'col-lg-8';
		}elseif ('sidebar-content' === euthenia_get_layout() ) {
			$blog_content_width[] = 'col-lg-8 order-last';
		}else{
			$blog_content_width[] = 'col-lg-12';
		}

		// return the $classes array
    	echo implode( ' ', $blog_content_width );
	}
endif;

if(function_exists('vc_add_param')){
	vc_add_param(
		'vc_row',
		array(
			"type" => "dropdown",
			"heading" => esc_html__('Setup Full width For Row', 'euthenia'),
			"param_name" => "fullwidth",
			"value" => array(   
			                esc_html__('No', 'euthenia') => 'no',  
			                esc_html__('Yes', 'euthenia') => 'yes',                                                                                
			              ),
			"description" => esc_html__("Select Full width for row : yes or not, Default: No fullwidth", 'euthenia'),      
        )
    );   

	// Add new Param in Column	
	vc_add_param('vc_column',array(
		  "type" => "dropdown",
		  "heading" => esc_html__('Animate Column', 'euthenia'),
		  "param_name" => "animate",
		  "value" => array(   
							esc_html__('None', 'euthenia') => 'none', 
							esc_html__('Move Top', 'euthenia') => 'topmove',
							esc_html__('Move Bottom', 'euthenia') => 'bottommove', 
							esc_html__('Move Left', 'euthenia') => 'leftmove', 
							esc_html__('Move Right', 'euthenia') => 'rightmove',  
						  ),
		  "description" => esc_html__("Select Animate , Default: None", 'euthenia'),      
		) 
    );
	vc_add_param('vc_column',array(
		  "type" => "textfield",
		  "heading" => esc_html__('Animation Distance', 'euthenia'),
		  "param_name" => "distance",
		  "value" => "",
		  "description" => esc_html__("Input distance show column. Example: 50, 60, etc", 'euthenia'), 
		  "dependency"  => array( 'element' => 'animate', 'value' => array( 'topmove', 'bottommove', 'leftmove', 'rightmove' ) ),     
		) 
    );
    vc_add_param('vc_column',array(
		  "type" => "textfield",
		  "heading" => esc_html__('Animation Time', 'euthenia'),
		  "param_name" => "time",
		  "value" => "",
		  "description" => esc_html__("Input time show column. Example: 0.8, 0.9, ...", 'euthenia'),   
		  "dependency"  => array( 'element' => 'animate', 'value' => array('topmove', 'bottommove', 'leftmove', 'rightmove' ) ),   
		) 
    );  
    vc_add_param('vc_column',array(
		  "type" => "textfield",
		  "heading" => esc_html__('Animation Time After.', 'euthenia'),
		  "param_name" => "after",
		  "value" => "",
		  "description" => esc_html__("Input time show column. Example: 0.1, 0.2 ...", 'euthenia'),   
		  "dependency"  => array( 'element' => 'animate', 'value' => array('topmove', 'bottommove', 'leftmove', 'rightmove' ) ),   
		) 
    );  
}

if(function_exists('vc_remove_param')){
    vc_remove_param( "vc_row", "parallax" );
    vc_remove_param( "vc_row", "parallax_image" );
    vc_remove_param( "vc_row", "video_bg" );
    vc_remove_param( "vc_row", "video_bg_url" );
    vc_remove_param( "vc_row", "video_bg_parallax" );
    vc_remove_param( "vc_row", "parallax_speed_bg" );
    vc_remove_param( "vc_row", "parallax_speed_video" );
    vc_remove_param( "vc_row", "gap" );
    vc_remove_param( "vc_column", "css_animation" ); 
    vc_remove_param( "vc_column", "video_bg" ); 
    vc_remove_param( "vc_column", "video_bg_url" ); 
    vc_remove_param( "vc_column", "parallax" ); 
    vc_remove_param( "vc_column", "video_bg_parallax" ); 
    vc_remove_param( "vc_column", "parallax_image" ); 
    vc_remove_param( "vc_column", "parallax_speed_bg" ); 
    vc_remove_param( "vc_column", "parallax_speed_video" ); 
}	