<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Euthenia
 */

/** Posts Navigation **/
if ( ! function_exists( 'euthenia_posts_navigation' ) ) :
    function euthenia_posts_navigation($prev = '<i class="fa fa-angle-left"></i>', $next = '<i class="fa fa-angle-right"></i>', $pages='') {
        global $wp_query, $wp_rewrite;
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
        if($pages==''){
            global $wp_query;
            $pages = $wp_query->max_num_pages;
            if(!$pages)
            {
                $pages = 1;
            }
        }
        $pagination = array(
            'base'          => str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
            'format'        => '',
            'current'       => max( 1, get_query_var('paged') ),
            'total'         => $pages,
            'prev_text'     => $prev,
            'next_text'     => $next,
            'type'          => 'list',
            'end_size'      => 3,
            'mid_size'      => 3
        );
        $return =  paginate_links( $pagination );
        echo str_replace( "<ul class='page-numbers'>", '<ul class="page-pagination">', $return );
    }
endif;

/**** Change length of the excerpt ****/
if ( ! function_exists( 'euthenia_excerpt_length' ) ) :
    function euthenia_excerpt_length() {

        if(euthenia_get_option('excerpt_length')){
            $limit = euthenia_get_option('excerpt_length');
        }else{
            $limit = 30;
        }
        $excerpt = explode(' ', get_the_excerpt(), $limit);

        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return $excerpt;

    }
endif;

//Custom comment list
if ( ! function_exists( 'euthenia_comment_list' ) ) :
    function euthenia_comment_list($comment, $args, $depth) {

        $GLOBALS['comment'] = $comment; ?>

        <div class="section">
            <?php echo get_avatar($comment,$size='40',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536' ); ?>
          <h6><?php printf('%s', get_comment_author()) ?> <small><?php comment_date('M d, Y'); ?> <?php esc_html_e('at','euthenia'); ?> <?php the_time(); ?></small></h6>
                <?php if ($comment->comment_approved == '0'){ ?>
                     <p><em><?php esc_html_e('Your comment is awaiting moderation.','euthenia') ?></em></p>
                <?php }else{ ?>
                <?php comment_text() ?>
             <?php } ?>     
             <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
           
        </div> 
        <div class="separator-wrap pt-4 pb-4">  
          <span class="separator"><span class="separator-line dashed"></span></span>
        </div>

        <?php
    }
endif;

/** Move comment field bottom **/
function wpb_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

//Generate custom search form
function euthenia_search_form( $form ) {
    $form = '<form role="search" method="get" id="search-form" class="subscribe-box ajax-form" action="' . esc_url( home_url( '/' ) ) . '" >
    <input type="search" class="hover-target" placeholder="' . esc_html__( 'type here &hellip;', 'euthenia' ) . '" value="' . get_search_query() . '" name="s" /></label>
	<button type="submit" class="subscribe-1 hover-target">Search</button>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'euthenia_search_form' );