<?php
/**
 * Hooks for importer
 *
 * @package Euthenia
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function euthenia_importer() {
	return array(
		array(
			'name'       => 'Main Demo',
			'preview'    => get_template_directory_uri().'/inc/backend/data/screenshot.jpg',
			'content'    => get_template_directory_uri().'/inc/backend/data/demo-content.xml',
			'customizer' => get_template_directory_uri().'/inc/backend/data/customizer.dat',
			'widgets'    => get_template_directory_uri().'/inc/backend/data/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Slider Center Title',
				'blog'       => 'Our Stories',
			),
			'menus'      => array(
				'primary'   => 'main-menu',
			)
		),		
	);
}

add_filter( 'soo_demo_packages', 'euthenia_importer', 30 );