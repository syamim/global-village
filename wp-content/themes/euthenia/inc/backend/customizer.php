<?php
/**
 * Theme customizer
 *
 * @package Euthenia
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Euthenia_Customize {
	/**
	 * Customize settings
	 *
	 * @var array
	 */
	protected $config = array();

	/**
	 * The class constructor
	 *
	 * @param array $config
	 */
	public function __construct( $config ) {
		$this->config = $config;

		if ( ! class_exists( 'Kirki' ) ) {
			return;
		}

		$this->register();
	}

	/**
	 * Register settings
	 */
	public function register() {

		/**
		 * Add the theme configuration
		 */
		if ( ! empty( $this->config['theme'] ) ) {
			Kirki::add_config(
				$this->config['theme'], array(
					'capability'  => 'edit_theme_options',
					'option_type' => 'theme_mod',
				)
			);
		}

		/**
		 * Add panels
		 */
		if ( ! empty( $this->config['panels'] ) ) {
			foreach ( $this->config['panels'] as $panel => $settings ) {
				Kirki::add_panel( $panel, $settings );
			}
		}

		/**
		 * Add sections
		 */
		if ( ! empty( $this->config['sections'] ) ) {
			foreach ( $this->config['sections'] as $section => $settings ) {
				Kirki::add_section( $section, $settings );
			}
		}

		/**
		 * Add fields
		 */
		if ( ! empty( $this->config['theme'] ) && ! empty( $this->config['fields'] ) ) {
			foreach ( $this->config['fields'] as $name => $settings ) {
				if ( ! isset( $settings['settings'] ) ) {
					$settings['settings'] = $name;
				}

				Kirki::add_field( $this->config['theme'], $settings );
			}
		}
	}

	/**
	 * Get config ID
	 *
	 * @return string
	 */
	public function get_theme() {
		return $this->config['theme'];
	}

	/**
	 * Get customize setting value
	 *
	 * @param string $name
	 *
	 * @return bool|string
	 */
	public function get_option( $name ) {

		$default = $this->get_option_default( $name );

		return get_theme_mod( $name, $default );
	}

	/**
	 * Get default option values
	 *
	 * @param $name
	 *
	 * @return mixed
	 */
	public function get_option_default( $name ) {
		if ( ! isset( $this->config['fields'][ $name ] ) ) {
			return false;
		}

		return isset( $this->config['fields'][ $name ]['default'] ) ? $this->config['fields'][ $name ]['default'] : false;
	}
}

/**
 * This is a short hand function for getting setting value from customizer
 *
 * @param string $name
 *
 * @return bool|string
 */
function euthenia_get_option( $name ) {
	global $euthenia_customize;

	$value = false;

	if ( class_exists( 'Kirki' ) ) {
		$value = Kirki::get_option( 'euthenia', $name );
	} elseif ( ! empty( $euthenia_customize ) ) {
		$value = $euthenia_customize->get_option( $name );
	}

	return apply_filters( 'euthenia_get_option', $value, $name );
}

/**
 * Get default option values
 *
 * @param $name
 *
 * @return mixed
 */
function euthenia_get_option_default( $name ) {
	global $euthenia_customize;

	if ( empty( $euthenia_customize ) ) {
		return false;
	}

	return $euthenia_customize->get_option_default( $name );
}

/**
 * Move some default sections to `general` panel that registered by theme
 *
 * @param object $wp_customize
 */
function euthenia_customize_modify( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->panel     = 'general';
	$wp_customize->get_section( 'static_front_page' )->panel = 'general';
}

add_action( 'customize_register', 'euthenia_customize_modify' );


/**
 * Get customize settings
 *
 * Priority (Order) WordPress Live Customizer default: 
 * @link https://developer.wordpress.org/themes/customize-api/customizer-objects/
 *
 * @return array
 */
function euthenia_customize_settings() {
	/**
	 * Customizer configuration
	 */

	$settings = array(
		'theme' => 'euthenia',
	);

	$panels = array(
		'general'        => array(
			'priority'   => 5,
			'title'      => esc_html__( 'General', 'euthenia' ),
		),
        'header'         => array(
            'title'      => esc_html__( 'Header', 'euthenia' ),
            'priority'   => 9,
            'capability' => 'edit_theme_options',
        ),
        'blog'           => array(
            'title'      => esc_html__( 'Blog', 'euthenia' ),
            'priority'   => 10,
            'capability' => 'edit_theme_options',
        ),
        'footer'         => array(
            'title'      => esc_html__( 'Footer', 'euthenia' ),
            'priority'   => 12,
            'capability' => 'edit_theme_options',
        ),
	);

	$sections = array(
        'logo_header'           => array(
            'title'       => esc_html__( 'Logo', 'euthenia' ),
            'description' => '',
            'priority'    => 17,
            'capability'  => 'edit_theme_options',
            'panel'       => 'header',
        ),
        'header_styling'           => array(
            'title'       => esc_html__( 'Styling', 'euthenia' ),
            'description' => '',
            'priority'    => 19,
            'capability'  => 'edit_theme_options',
            'panel'       => 'header',
        ),
        'blog_page'           => array(
            'title'       => esc_html__( 'Blog Page', 'euthenia' ),
            'description' => '',
            'priority'    => 10,
            'capability'  => 'edit_theme_options',
            'panel'       => 'blog',
        ),
        'single_post'           => array(
            'title'       => esc_html__( 'Single Post', 'euthenia' ),
            'description' => '',
            'priority'    => 10,
            'capability'  => 'edit_theme_options',
            'panel'       => 'blog',
        ),
        'main_footer'           => array(
            'title'       => esc_html__( 'Footer Content', 'euthenia' ),
            'description' => '',
            'priority'    => 20,
            'capability'  => 'edit_theme_options',
            'panel'       => 'footer',
        ),
        'footer_styling'           => array(
            'title'       => esc_html__( 'Footer Styling', 'euthenia' ),
            'description' => '',
            'priority'    => 21,
            'capability'  => 'edit_theme_options',
            'panel'       => 'footer',
        ),
        'typography'           => array(
            'title'       => esc_html__( 'Typography', 'euthenia' ),
            'description' => '',
            'priority'    => 15,
            'capability'  => 'edit_theme_options',
        ),
        'styling'           => array(
            'title'       => esc_html__( 'Styling', 'euthenia' ),
            'description' => '',
            'priority'    => 15,
            'capability'  => 'edit_theme_options',
        ),
	);

	$fields = array(
        //Logo
        'logo'         => array(
            'type'     => 'image',
            'label'    => esc_attr__( 'Logo Image', 'euthenia' ),
            'section'  => 'logo_header',
            'default'  => trailingslashit( get_template_directory_uri() ) . 'img/logo.png',
            'priority' => 3,
        ),
        'logo_width'     => array(
            'type'     => 'number',
            'label'    => esc_html__( 'Logo Width(px)', 'euthenia' ),
            'section'  => 'logo_header',
            'priority' => 4,
            'output'    => array(
                array(
                    'element'  => '.logo-wrap img',
                    'property' => 'width',
                    'units'    => 'px'
                ),
            ),
        ),
        'logo_height'    => array(
            'type'     => 'number',
            'label'    => esc_html__( 'Logo Height(px)', 'euthenia' ),
            'section'  => 'logo_header',
            'priority' => 5,
            'output'    => array(
                array(
                    'element'  => '.logo-wrap img',
                    'property' => 'height',
                    'units'    => 'px'
                ),
            ),
        ),
        'logo_position'  => array(
            'type'     => 'spacing',
            'label'    => esc_html__( 'Logo Margin', 'euthenia' ),
            'section'  => 'logo_header',
            'priority' => 6,
            'default'  => array(
                'top'    => '0',
                'bottom' => '0',
                'left'   => '0',
                'right'  => '0',
            ),
            'output'    => array(
                array(
                    'element'  => '.logo-wrap',
                    'property' => 'margin',
                ),
            ),
        ),
        //Styling
        'current_item_name_shadow'     => array(
            'type'        => 'toggle',
            'label'       => esc_attr__( 'Curent Page Name Shadow On/Off?', 'euthenia' ),
            'section'     => 'header_styling',
            'default'     => '1',
            'priority'    => 9,
        ),
        'bg_header'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Background Header', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.cd-header',
                    'property' => 'background'
                ),
            ),
        ),
        'bg_header_sc'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Background Header Scroll', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.cd-header.is-fixed',
                    'property' => 'background'
                ),
            ),
        ),
        'bg_menu'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Background Navigation', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.nav:after',
                    'property' => 'background'
                ),
            ),
        ),
        'color_menu'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Color Link Menu', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.nav__list-item a',
                    'property' => 'color'
                ),
            ),
        ),
        'color_smenu'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Color Link Dropdown Menu', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.nav__list .sub-links li a',
                    'property' => 'color'
                ),
            ),
        ),
        'color_ac_smenu'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Color Link Hover and Active Menu', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.nav__list-item a:hover, .nav__list-item.active-nav a, .nav__list-item a:not([href]):not([tabindex])',
                    'property' => 'color'
                ),
            ),
        ),
        'color_current_page'    => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Color Current Page Name', 'euthenia' ),
            'section'  => 'header_styling',
            'default'  => '',
            'priority' => 10,
            'output'    => array(
                array(
                    'element'  => '.curent-page-name-shadow',
                    'property' => 'color'
                ),
            ),
        ),
        'menu_typo'    => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Font Style', 'euthenia' ),
            'section'  => 'header_styling',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '300',
                'subsets'        => array( 'latin-ext' ),
                'font-size'      => '15px',
                'text-transform' => 'none',
            ),
        ),

        // Blog Page
        'blog_layout'           => array(
            'type'        => 'select',
            'label'       => esc_html__( 'Blog Layout', 'euthenia' ),
            'section'     => 'blog_page',
            'default'     => 'full-content',
            'priority'    => 9,
            'description' => esc_html__( 'Select default sidebar for the blog page.', 'euthenia' ),
            'choices'     => array(
                'content-sidebar' => esc_html__( 'Right Sidebar', 'euthenia' ),
                'sidebar-content' => esc_html__( 'Left Sidebar', 'euthenia' ),
                'full-content'    => esc_html__( 'Full Content', 'euthenia' ),
            )
        ),
        'text_shadow_blog'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Shadow Text On Left Screen', 'euthenia' ),
            'section'     => 'blog_page',
            'default'     => 'News',
            'priority'    => 12,
        ),
        'shadow_pagi_blog'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Shadow Text On Pagination', 'euthenia' ),
            'section'     => 'blog_page',
            'default'     => 'Our Stories',
            'priority'    => 16,
        ),
        'text_pagi_prev'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Text Prev On Pagination', 'euthenia' ),
            'section'     => 'blog_page',
            'default'     => 'Older News',
            'priority'    => 16,
        ),
        'text_pagi_next'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Text Next On Pagination', 'euthenia' ),
            'section'     => 'blog_page',
            'default'     => 'New News',
            'priority'    => 16,
        ),
        // Single Post
        'single_post_layout'           => array(
            'type'        => 'select',
            'label'       => esc_html__( 'Single Post Layout', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => 'content-sidebar',
            'priority'    => 10,
            'description' => esc_html__( 'Select default sidebar for the single post page.', 'euthenia' ),
            'choices'     => array(
                'content-sidebar' => esc_html__( 'Right Sidebar', 'euthenia' ),
                'sidebar-content' => esc_html__( 'Left Sidebar', 'euthenia' ),
                'full-content'    => esc_html__( 'Full Content', 'euthenia' ),
            ),
        ),
        'text_shadow_top'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Shadow Text On Left Screen', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => 'Stories',
            'priority'    => 12,
        ),
        'single_pagi'     => array(
            'type'        => 'toggle',
            'label'       => esc_attr__( 'Pagination On/Off?', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => '1',
            'priority'    => 13,
        ),
        'shadow_pagi'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Shadow Text On Pagination', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => 'Story',
            'priority'    => 16,
            'active_callback' => array(
                array(
                    'setting'  => 'single_pagi',
                    'operator' => '==',
                    'value'    => 1,
                ),
            ),
        ),
        'text_single_prev'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Text Prev On Pagination', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => 'Older Post',
            'priority'    => 16,
            'active_callback' => array(
                array(
                    'setting'  => 'single_pagi',
                    'operator' => '==',
                    'value'    => 1,
                ),
            ),
        ),
        'text_single_next'     => array(
            'type'        => 'text',
            'label'       => esc_attr__( 'Text Next On Pagination', 'euthenia' ),
            'section'     => 'single_post',
            'default'     => 'New Post',
            'priority'    => 16,
            'active_callback' => array(
                array(
                    'setting'  => 'single_pagi',
                    'operator' => '==',
                    'value'    => 1,
                ),
            ),
        ),

        // Footer Content
        'copyright'     => array(
            'type'        => 'textarea',
            'label'       => esc_attr__( 'Copyright', 'euthenia' ),
            'section'     => 'main_footer',
            'priority'    => 6,
        ),
        'socialfooter_switch'     => array(
            'type'        => 'toggle',
            'label'       => esc_attr__( 'Social On/Off?', 'euthenia' ),
            'section'     => 'main_footer',
            'default'     => '1',
            'priority'    => 9,
        ),
        'footer_socials'     => array(
            'type'     => 'repeater',
            'label'    => esc_html__( 'Socials Network', 'euthenia' ),
            'section'  => 'main_footer',
            'priority' => 10,
            'active_callback' => array(
                array(
                    'setting'  => 'socialfooter_switch',
                    'operator' => '==',
                    'value'    => 1,
                ),
            ),
            'row_label' => array(
                'type' => 'field',
                'value' => esc_attr__('social', 'euthenia' ),
                'field' => 'social_name',
            ),
            'default'  => array(),
            'fields'   => array(
                'social_name' => array(
                    'type'        => 'text',
                    'label'       => esc_html__( 'Social network name', 'euthenia' ),
                    'description' => esc_html__( 'This will be the social network name', 'euthenia' ),
                    'default'     => '',
                ),
                'social_link' => array(
                    'type'        => 'text',
                    'label'       => esc_html__( 'Link url', 'euthenia' ),
                    'description' => esc_html__( 'This will be the social link', 'euthenia' ),
                    'default'     => '',
                ),
            ),
        ),

        //Footer Styling
        'color_footer' => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Color Text Footer', 'euthenia' ),
            'section'  => 'footer_styling',
            'priority' => 2,
            'output'    => array(
                array(
                    'element'  => '.social-fixed a, .copyr, .copyr a',
                    'property' => 'color',
                ),
            ),
        ),

        // Typography
        'body_typo'    => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Body', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '300',
                'font-size'      => '16px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading1_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 1', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '40px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading2_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 2', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '30px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading3_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 3', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '22px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading4_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 4', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '20px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading5_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 5', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '18px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),
        'heading6_typo'                           => array(
            'type'     => 'typography',
            'label'    => esc_html__( 'Heading 6', 'euthenia' ),
            'section'  => 'typography',
            'priority' => 10,
            'default'  => array(
                'font-family'    => 'Muli',
                'variant'        => '700',
                'font-size'      => '16px',
                'line-height'    => '1',
                'letter-spacing' => '0',
                'subsets'        => array( 'latin-ext' ),
                'color'          => '#1d1d1d',
                'text-transform' => 'none',
            ),
        ),

        //Styling
        'animation_load'     => array(
            'type'        => 'toggle',
            'label'       => esc_attr__( 'Animation Load On/Off?', 'euthenia' ),
            'section'     => 'styling',
            'default'     => '1',
            'priority'    => 9,
        ),
        'bg_body'      => array(
            'type'     => 'color',
            'label'    => esc_html__( 'Background Body', 'euthenia' ),
            'section'  => 'styling',
            'default'  => '',
            'priority' => 10,
            'output'   => array(
                array(
                    'element'  => 'body',
                    'property' => 'background-color',
                ),
            ),
        ),

	);
	$settings['panels']   = apply_filters( 'euthenia_customize_panels', $panels );
	$settings['sections'] = apply_filters( 'euthenia_customize_sections', $sections );
	$settings['fields']   = apply_filters( 'euthenia_customize_fields', $fields );

	return $settings;
}

$euthenia_customize = new Euthenia_Customize( euthenia_customize_settings() );