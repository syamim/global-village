<?php
/**
 * Registering meta boxes
 *
 * Using Meta Box plugin: http://www.deluxeblogtips.com/meta-box/
 *
 * @see https://docs.metabox.io/
 *
 * @param array $meta_boxes Default meta boxes. By default, there are no meta boxes.
 *
 * @return array All registered meta boxes
 */
function euthenia_register_meta_boxes( $meta_boxes ) {

	$meta_boxes[] = array(
		'id'       => 'page_detail',
		'title'    => __( 'Page Details', 'euthenia' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(
            array(
                'id'       => 'page_layout',
                'name'     => 'Layout',
                'type'     => 'image_select',
                'options'  => array(
                    'full-content'    => get_template_directory_uri() . '/inc/backend/images/full.png',
                    'content-sidebar' => get_template_directory_uri() . '/inc/backend/images/right.png',
                    'sidebar-content' => get_template_directory_uri() . '/inc/backend/images/left.png',
                ),
                'std'      => 'full-content'
            ),
			array(
				'name'             => esc_html__( 'Parallax Text', 'euthenia' ),
				'id'               => 'parallax_text',
				'type'             => 'text',	
			),	
			array(
				'name'             => esc_html__( 'Subheader', 'euthenia' ),
				'id'               => 'subheader',
				'type'             => 'checkbox',	
			),
			array(
				'name'             => esc_html__( 'Subtitle On Subheader', 'euthenia' ),
				'id'               => 'subtitle',
				'type'             => 'textarea',	
			),	
		),
	);

	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'euthenia_register_meta_boxes' );
