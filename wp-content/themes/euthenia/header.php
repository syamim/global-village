 <?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Euthenia
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="<?php if(euthenia_get_option('animation_load')==true){echo 'animsition';} ?>">
		<header class="cd-header">
			<div class="header-wrapper">
				<div class="logo-wrap">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="hover-target animsition-link"><img src="<?php echo esc_url( euthenia_get_option('logo') ); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>"></a>
				</div>
				<div class="nav-but-wrap">
					<div class="menu-icon hover-target">
						<span class="menu-icon__line menu-icon__line-left"></span>
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line menu-icon__line-right"></span>
					</div>					
				</div>					
			</div>				
		</header>

		<div class="nav">
			<div class="nav__content">
				<?php 
					$menu_items = wp_get_nav_menu_items( 'main-menu' );
					$this_item = current( wp_filter_object_list( $menu_items, array( 'object_id' => get_queried_object_id() ) ) );
 				?>
 				<?php if(euthenia_get_option('current_item_name_shadow')==true){ ?>
				<div class="curent-page-name-shadow">
					<?php if ( is_single() && get_post_type() == 'post' ) { echo esc_html_e('post','euthenia'); }
					elseif (is_single() && get_post_type() == 'portfolio' ) { echo esc_html_e('project','euthenia'); }
					elseif (is_archive() ) { echo esc_html_e('archive','euthenia'); }
					elseif (is_search() ) { echo esc_html_e('search','euthenia'); }
					elseif ($this_item->attr_title != ''){ echo esc_attr($this_item->attr_title); }
					else echo esc_attr($this_item->title); ?>
				</div>
				<?php } ?>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
                    	'items_wrap'     => '<ul class="nav__list">%3$s</ul>',
					) );
				?>
			</div>
		</div>	