<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Euthenia
 */

get_header();
?>
<?php if(euthenia_get_option('text_shadow_blog')!=''){ ?><div class="shadow-title parallax-top-shadow"><?php echo esc_attr(euthenia_get_option('text_shadow_blog')); ?></div><?php } ?>
<div class="section padding-page-top padding-bottom over-hide z-bigger">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">
			<div class="col-12 parallax-fade-top">
				<h1><?php echo get_the_title(get_option('page_for_posts')); ?></h1>
			</div>
			<div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3">
				<p><?php esc_html_e('full of stuff','euthenia'); ?></p>
			</div>
		</div>
	</div>
</div>
<div class="section padding-bottom-big z-bigger over-hide">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">

			<div class="content-area <?php euthenia_content_columns(); ?>">
				<ul class="case-study-wrapper vertical-blog">

				<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_type() );

					endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

				</ul><!-- #main -->
			</div><!-- #primary -->

				<?php get_sidebar(); ?>
			
		</div>
	</div>	
</div>

<?php if ( have_posts() ) : ?>
<ul class="case-study-images">
		<?php

		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-thumbnail', get_post_type() );

		endwhile; ?>
	
</ul>
<?php endif; ?>

<?php if ($wp_query->max_num_pages > 1) { ?>
<div class="section padding-top-bottom over-hide z-bigger background-dark-3 footer">
	<div class="shadow-on-footer" data-scroll-reveal="enter bottom move 30px over 0.5s after 0.1s"><?php echo esc_attr(euthenia_get_option('shadow_pagi_blog')); ?></div>
	<div class="container" data-scroll-reveal="enter bottom move 20px over 0.5s after 0.4s">
		<div class="row">
			<div class="col-12 text-center z-bigger py-5">
				<div class="footer-lines custom-footer-lines">
					<?php previous_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_prev')).'</h4>' ); ?>
                    <?php next_posts_link( '<h4>'.esc_attr(euthenia_get_option('text_pagi_next')).'</h4>' ); ?>
				</div>
			</div>
		</div>
	</div>		
</div>
<?php } ?>
<?php
get_footer();