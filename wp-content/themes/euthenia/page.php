<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Euthenia
 */
$sub = get_post_meta(get_the_ID(), 'subtitle', true);
$subhead = get_post_meta(get_the_ID(), 'subheader', true);
$ptext = get_post_meta(get_the_ID(), 'parallax_text', true);
get_header();
?>

<?php if($ptext!=''){ ?><div class="shadow-title parallax-top-shadow"><?php echo esc_attr($ptext); ?></div><?php } ?>

<?php if($subhead==true){ ?>
<div class="section padding-page-top padding-bottom over-hide z-bigger">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">
			<div class="col-12 parallax-fade-top">
				<h1><?php the_title(); ?></h1>
			</div>
			<?php if($sub!=''){ ?>
			<div class="offset-1 col-11 parallax-fade-top mt-2 mt-sm-3">
				<p><?php echo esc_attr($sub); ?></p>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } ?>

    <?php 
    	if(euthenia_get_layout() =='full-content') :

        while ( have_posts() ) : the_post();

        the_content();

    endwhile; else : ?>

<div class="section padding-bottom-big z-bigger over-hide">
	<div class="container z-bigger">
		<div class="row page-title px-5 px-xl-2">

			<div class="eontent-area <?php euthenia_content_columns(); ?>">

				<?php
	            while ( have_posts() ) :
	                the_post();

	                get_template_part( 'template-parts/content', 'page' );

	                // If comments are open or we have at least one comment, load up the comment template.
	                if ( comments_open() || get_comments_number() ) :
	                    comments_template();
	                endif;

	            endwhile; // End of the loop.
            ?>

			</div><!-- #primary -->

				<?php get_sidebar(); ?>
			
		</div>
	</div>	
</div>
<?php endif; ?>
<?php
get_footer();