<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Euthenia
 */

?>
		<?php if(euthenia_get_option('socialfooter_switch')==true){ ?>
		<div class="social-fixed">          
			<?php $socials = euthenia_get_option( 'footer_socials', array() ); ?>
	        <?php foreach ( $socials as $social ) { ?>                      
			     <a class="hover-target" href="<?php echo esc_url($social['social_link']); ?>"><?php echo esc_attr($social['social_name']); ?></a>       
	        <?php } ?>
		</div>
		<?php } ?>

		<?php if(euthenia_get_option('copyright')!=''){ ?>
			<div class="copyr">
				<?php echo wp_kses( euthenia_get_option('copyright'), wp_kses_allowed_html('post') ); ?>
			</div>
		<?php } ?>
	
	<div class="scroll-to-top hover-target"></div>
	
	<!-- Page cursor
	================================================== -->
	
    <div class='cursor' id="cursor"></div>
    <div class='cursor2' id="cursor2"></div>
    <div class='cursor3' id="cursor3"></div>

</div>

<?php wp_footer(); ?>

</body>
</html>
