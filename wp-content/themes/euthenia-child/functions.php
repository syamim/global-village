<?php
add_action( 'wp_enqueue_scripts', 'euthenia_child_enqueue_styles' );
function euthenia_child_enqueue_styles() {
    $parent_style = 'parent-style'; // This is 'euthenia-style' for the Euthenia theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
}
