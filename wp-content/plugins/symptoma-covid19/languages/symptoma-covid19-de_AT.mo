��          t      �                 "     2     B     O     k     �  X   �  -   �       �  +     �      �                )     E     _  \   ~  -   �     	                                    
   	                     BANNER_LINK_TEXT BANNER_LINK_URL BANNER_SUBTITLE BANNER_TITLE Helmuth Lammer, Thomas Lutz Symptoma COVID-19 Chatbot TITLE This plugin integrates the Symptoma chatbot to check COVID-19 symptoms on a global basis https://github.com/symptoma/covid19-wordpress https://www.symptoma.com Project-Id-Version: Symptoma COVID-19 Chatbot
PO-Revision-Date: 2020-08-12 14:39+0200
Last-Translator: 
Language-Team: 
Language: de_AT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: class-symptoma-covid19-admin.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Info https://www.symptoma.at/covid-19 Test starten COVID-19 Helmuth Lammer, Thomas Lutz Symptoma COVID-19 Chatbot Symptoma COVID-19 Test Chatbot Dieses Plugin integriert den Symptoma Chatbot, um COVID-19 Symptome weltweit zu überprüfen https://github.com/symptoma/covid19-wordpress https://www.symptoma.com 